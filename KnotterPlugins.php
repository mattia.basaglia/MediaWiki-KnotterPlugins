<?php


if( !defined( 'MEDIAWIKI' ) ) {
        echo( "This is an extension to the MediaWiki package and cannot be run standalone.\n" );
        die( -1 );
}


$wgExtensionCredits['parserhook'][] = array(
        'path' => __FILE__,
        'name' => "knotter_plugin",
        'descriptionmsg' => "Load plugins for Knotter",
        'version' => 1,
        'author' => "Mattia Basaglia",
        //'url' => "http://dubdubdub.example.com",
);


$wgHooks['ParserFirstCallInit'][] = 'KnotterPluginParserInit';


global $wg_KP_Download_Url;
global $wg_KP_Browse_Url;
global $wg_KP_Plugin_Dir;
$wg_KP_Download_Url='http://plugins.knotter.mattbas.org/packages';
$wg_KP_Browse_Url='http://plugins.knotter.mattbas.org/plugins';
$wg_KP_Plugin_Dir='/home/mattbas6/public_html/knotter_plugins/plugins';

function KnotterPluginParserInit(Parser &$parser)
{
    $parser->setHook('knotter_plugin','KnotterPluginParserHook');
    $parser->setHook('knotter_plugin_list','KnotterPluginListParserHook');
    return true; // continue hook processing
}

/**
 * \note json_decode requires that property names to be enclodes by quotes
 */
function read_plugin($file)
{
    $plugin_data = json_decode(file_get_contents($file),true);
    
    if ( !isset($plugin_data['name']) )
    {
        //return false;
        $match = array();
        if ( preg_match("/plugin_(.+)\.json/",$file,$match) )
            $plugin_data['name'] = $match[1];
        else
            return false;
    }
        
    $plugin_data['name'] = ucwords($plugin_data['name']);
    
    if ( !isset($plugin_data['type']) )
        $plugin_data['type'] = 'script';
    
    $plugin_data['type'] = strtolower($plugin_data['type']);
    
    if ( !isset($plugin_data['category']) )
    {
        if ( $plugin_data['type'] == 'cusp' )
            $plugin_data['category'] = 'Cusp';
        else if ( $plugin_data['type'] == 'crossing' )
            $plugin_data['category'] = 'Crossing';
        else
            $plugin_data['category'] = 'Other';
    }
        
    return $plugin_data;
}

function KnotterPluginParserHook ( $text, array $args, Parser $parser, PPFrame $frame )
{   
    global $wg_KP_Browse_Url;
    global $wg_KP_Download_Url;
    global $wg_KP_Plugin_Dir;
    
    $wikicode = "";
    $plugin=trim($text);
    $plugin_dir = isset($args["dir"]) ? $args["dir"] : $plugin;
    $plugin_file = "$wg_KP_Plugin_Dir/$plugin_dir/plugin_$plugin.json";
    $plugin_data = read_plugin($plugin_file);
    
    if ( $plugin_data )
    {
        
        $wikicode .= "{{Plugin|";
        $wikicode .= "name={$plugin_data['name']}|";
        $wikicode .= "download=$wg_KP_Download_Url/$plugin.tar.gz|";
        $wikicode .= "browse=$wg_KP_Browse_Url/$plugin_dir|";
        $wikicode .= "version={$plugin_data['requires']}|";
        $wikicode .= "plugin_version={$plugin_data['version']}|";
        $wikicode .= "type=".ucfirst($plugin_data['type'])."|";
        if ( isset($args["screenshot"]) )
            $wikicode .= "screenshot={$args['screenshot']}|";
        $wikicode .= "category={$plugin_data['category']}|";
        $wikicode .= "author={$plugin_data['author']}|";
        $wikicode .= "license={$plugin_data['license']}|";
        $wikicode .= "}}\n\n";
        $wikicode .= $plugin_data["description"]."\n";
    }
    else
        $wikicode="{{ambox|type=error|text=Plugin $text not found}}";

        /*$wikicode .= "\n\n==plugin_data==\n<pre><nowiki>".print_r($plugin_data, true)."</nowiki></pre>";
        $cnt = file_get_contents($plugin_file);
        $wikicode .= "\n\n==file_get_contents==\n<pre><nowiki>$cnt</nowiki></pre>";
        $wikicode .= "\n\n==json_decode==\n<pre><nowiki>".json_decode($cnt,true)."</nowiki></pre>";*/

    return  $parser->recursiveTagParse($wikicode,$frame);
} 


function KnotterPluginListParserHook ( $text, array $args, Parser $parser, PPFrame $frame )
{
    global $wg_KP_Plugin_Dir;
    
    $plugins = array();
    
    if ( is_dir($wg_KP_Plugin_Dir) )
    {
        $dirs = scandir($wg_KP_Plugin_Dir);
        
        foreach($dirs as $dir)
        {
            if ( strpos($dir,".") === false && is_dir("$wg_KP_Plugin_Dir/$dir") )
            {
                $files = scandir("$wg_KP_Plugin_Dir/$dir");
                foreach($files as $file)
                {
                    if ( preg_match("/plugin_.+\.json/",$file) )
                    {
                        $plugin = read_plugin("$wg_KP_Plugin_Dir/$dir/$file");
                        if ( $plugin )
                        {
                            if ( !isset($plugins[$plugin["category"]]) )
                                $plugins[$plugin["category"]] = array();
                            $plugins[$plugin["category"]] []= $plugin["name"];
                        }
                    }
                }
            }
        }
    }
    
    $wikicode = "";
    
    foreach ( $plugins as $cat => $plugs )
    {
        $wikicode .= "==$cat==\n";
        foreach ( $plugs as $p )
            $wikicode .= "* {{rl|$p}}\n";
    }
    
    return  $parser->recursiveTagParse($wikicode,$frame);
}